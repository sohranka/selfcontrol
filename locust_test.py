from locust import HttpUser, TaskSet, task


@task(1)
def index(l):
    l.client.get("/week")


@task(2)
def index2(l):
    l.client.get("/day/23")
    l.client.get("/day/22")
    l.client.get("/day/24")
    l.client.get("/day/25")


new_post = {
    "title": "testChangeTask",
    "status": "done"
}


@task(3)
def index3(l):
    l.client.post("/task/update/23", json=new_post)

@task(5)
def index5(l):
    l.client.get("/task/delete/23")


@task(4)
def index4(l):
    l.client.get("/")


class UserBehavior(TaskSet):
    tasks = {index: 3}


class WebsiteUser(HttpUser):
    tasks = {index: 6, index2: 6, index3: 3, index4:8, index5:2}
    min_wait = 5000
    max_wait = 9000
